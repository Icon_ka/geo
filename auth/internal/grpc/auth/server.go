package auth

import (
    auth "gitlab.com/Icon_ka/geo/auth/protos/gen/go"
    "google.golang.org/grpc"
    "context"
    "log"
    "net"
)

type serverAPI struct {
    auth.AuthServer
}

func NewServer() {
    listen, err := net.Listen("tcp", "localhost:1234")
    if err != nil {
        log.Fatalf("Ошибка при прослушивании порта: %v", err)
    }
    server := grpc.NewServer()
    RegisterServer(server)

    log.Println("Запуск gRPC сервера...")
    if err := server.Serve(listen); err != nil {
        log.Fatalf("Ошибка при запуске сервера: %v", err)
    }
}

func RegisterServer(gRPC *grpc.Server) {
    auth.RegisterAuthServer(gRPC, &serverAPI{})
}

func (s *serverAPI) Register(ctx context.Context, user *auth.RegisterRequest) (*auth.RegisterResponse, error) {
    panic("Implement")
}

func (s *serverAPI) Login(ctx context.Context, user *auth.LoginRequest) (*auth.LoginResponse, error) {
    panic("implement")
}
