package main

import (
    "syscall"
    "os/signal"
    "os"
    "gitlab.com/Icon_ka/geo/auth/internal/grpc/auth"
)

func main() {

    go func() {
        auth.NewServer()
    }()

    stop := make(chan os.Signal, 1)
    signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)

    <-stop
}
