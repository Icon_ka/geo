package run

import (
    "os"
    "net/http"
    "log"
    "time"
    "syscall"
    "os/signal"
    "context"
    "fmt"
    "github.com/golang-migrate/migrate/v4/database/postgres"
    "database/sql"
    "go.uber.org/zap"
    "gitlab.com/Icon_ka/geo/internal/controller"
    "gitlab.com/Icon_ka/geo/internal/router"
    "gitlab.com/Icon_ka/geo/internal/infrastructure/responder"
    "github.com/golang-migrate/migrate/v4"
    "gitlab.com/Icon_ka/geo/internal/service/geo"
    "gitlab.com/Icon_ka/geo/internal/service"
    geo2 "gitlab.com/Icon_ka/geo/internal/repository/geo"
    metrics2 "gitlab.com/Icon_ka/geo/internal/metrics"
    "gitlab.com/Icon_ka/geo/internal/geocoder"
    "gitlab.com/Icon_ka/geo/internal/geocoder/rpc_geocoder"
    "gitlab.com/Icon_ka/geo/internal/geocoder/json_rpc_geocoder"
    "gitlab.com/Icon_ka/geo/internal/geocoder/gRPC_geocoder"
)

// Application - интерфейс приложения
type Application interface {
    Runner
    Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
    Run()
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
    Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
    srv      *http.Server
    Repo     *geo2.AddressRepository
    Services *geo.AddressServicer
}

func NewApp() *App {
    return &App{}
}

// Run - запуск приложения
func (a *App) Run() {
    go func() {
        log.Println("Starting server...")
        if err := a.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
            log.Fatalf("Server error: %v", err)
        }
    }()

    sigChan := make(chan os.Signal, 1)
    signal.Notify(sigChan, syscall.SIGINT)

    <-sigChan

    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    err := a.srv.Shutdown(ctx)
    if err != nil {
        log.Fatalf("Server shutdown error: %v", err)
    }

    <-ctx.Done()

    log.Println("Server stopped gracefully")
}

func (a *App) Bootstrap(options ...interface{}) Runner {
    time.Sleep(2 * time.Second)
    db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@db:%s/%s?sslmode=disable",
        os.Getenv("DB_USER"),
        os.Getenv("DB_PASSWORD"),
        os.Getenv("DB_PORT"),
        os.Getenv("DB_NAME"),
    ))
    if err != nil {
        log.Fatalf("Failed to open database: %v", err)
    }

    driver, err := postgres.WithInstance(db, &postgres.Config{})
    if err != nil {
        log.Fatalf("driver, err := postgres.WithInstance(db, &postgres.Config{}): %v", err)
    }

    m, err := migrate.NewWithDatabaseInstance(
        "file:///internal/migrations",
        "postgres",
        driver,
    )
    if err != nil {
        log.Fatalf("m, err := migrate.NewWithDatabaseInstance: %v", err)
    }

    err = m.Up()
    if err != nil {
        log.Fatalf("err = m.Up(): %v", err)
    }

    metrics := metrics2.NewMetrics()
    logger := zap.NewExample()

    var geoprovider geocoder.GeoProvider
    if os.Getenv("RPC_PROTOCOL") == "rpc" {
        go rpc_geocoder.NewDadataRPC()

        time.Sleep(2 * time.Second)

        cli := geocoder.RPCClientFactory{}

        geoprovider, err = cli.CreateClient()
        log.Println(err)
    } else if os.Getenv("RPC_PROTOCOL") == "json-rpc" {
        go json_rpc_geocoder.NewDadataJsonRPC()

        time.Sleep(2 * time.Second)

        cli := geocoder.JsonRPCClientFactory{}

        geoprovider = cli.CreateClient()
    } else if os.Getenv("RPC_PROTOCOL") == "grpc" {
        go gRPC_geocoder.NewDadatagRPC()

        time.Sleep(2 * time.Second)

        cli := geocoder.GRPCClientFactory{}

        geoprovider = cli.CreateClient()
    }

    resp := responder.NewResponder(logger)
    serv := service.NewServices(db, metrics, geoprovider)
    ctrl := controller.NewControllers(resp, *serv, metrics)
    r := router.NewApiRouter(ctrl)

    a.srv = &http.Server{
        Addr:    ":8080",
        Handler: r,
    }

    return a
}
