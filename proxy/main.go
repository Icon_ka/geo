package main

import (
    "gitlab.com/Icon_ka/geo/run"
    "log"
    "github.com/joho/godotenv"
    _ "github.com/golang-migrate/migrate/v4/source/file"
    _ "github.com/lib/pq"
)

func main() {
    err := godotenv.Load()
    if err != nil {
        log.Fatal("Ошибка при загрузке файла .env", err)
    }

    app := run.NewApp()

    app.Bootstrap().Run()
}
