package repository

import (
    "gitlab.com/Icon_ka/geo/internal/repository/geo"
    "database/sql"
    "gitlab.com/Icon_ka/geo/internal/metrics"
)

type Repository struct {
    AddressRepository geo.AddressRepository
}

func NewRepository(db *sql.DB, metrics *metrics.Metrics) *Repository {
    return &Repository{AddressRepository: geo.NewPostgresAddressRepository(db, metrics)}
}
