package geo

import "gitlab.com/Icon_ka/geo/internal/entities"

type AddressRepository interface {
    GetAddresses(query string) (entities.AddressDTO, error)
    SaveAddresses(address entities.AddressDTO, query string) error
}
