package geo

import (
    sq "github.com/Masterminds/squirrel"
    "database/sql"
    "gitlab.com/Icon_ka/geo/internal/metrics"
    "time"
    "github.com/prometheus/client_golang/prometheus"
    "gitlab.com/Icon_ka/geo/internal/entities"
)

type PostgresAddressRepository struct {
    db      *sql.DB
    builder sq.StatementBuilderType
    metrics *metrics.Metrics
}

func NewPostgresAddressRepository(db *sql.DB, metrics *metrics.Metrics) *PostgresAddressRepository {
    builder := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
    return &PostgresAddressRepository{db: db, builder: builder, metrics: metrics}
}

func (r *PostgresAddressRepository) GetAddresses(query string) (entities.AddressDTO, error) {

    startTime := time.Now()
    q := `SELECT hsa.search_history_id, a.data, a.lat, a.lon
              FROM history_search_address hsa
              JOIN address a ON hsa.address_id = a.id
              WHERE hsa.search_history_id IN (SELECT id FROM search_history WHERE similarity(query, $1) >= 0.7)`

    var addressDTO entities.AddressDTO
    var searchHistoryID int
    err := r.db.QueryRow(q, query).Scan(&searchHistoryID, &addressDTO.Data, &addressDTO.Lat, &addressDTO.Lon)
    if err != nil {
        return entities.AddressDTO{}, err
    }

    elapsedTime := time.Since(startTime)

    r.metrics.DatabaseTime.With(prometheus.Labels{"method": "POST", "path": "/api/address/search"}).Observe(elapsedTime.Seconds())
    return addressDTO, nil
}

func (r *PostgresAddressRepository) SaveAddresses(address entities.AddressDTO, query string) error {

    startTime := time.Now()
    q := `INSERT INTO search_history (query) VALUES ($1) RETURNING id`

    var sh_id int
    err := r.db.QueryRow(q, query).Scan(&sh_id)
    if err != nil {
        return err
    }

    q = `INSERT INTO address (data, lat, lon) VALUES ($1,$2,$3) RETURNING id`

    var ad_id int
    err = r.db.QueryRow(q, address.Data, address.Lat, address.Lon).Scan(&ad_id)
    if err != nil {
        return err
    }

    q = `INSERT INTO history_search_address (search_history_id, address_id) VALUES ($1, $2)`

    _, err = r.db.Exec(q, sh_id, ad_id)
    if err != nil {
        return err
    }

    elapsedTime := time.Since(startTime)

    r.metrics.DatabaseTime.With(prometheus.Labels{"method": "POST", "path": "/api/address/search"}).Observe(elapsedTime.Seconds())

    return nil
}
