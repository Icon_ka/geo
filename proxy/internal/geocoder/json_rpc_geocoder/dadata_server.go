package json_rpc_geocoder

import (
    "net/http"
    "strings"
    "encoding/json"
    "gitlab.com/Icon_ka/geo/internal/entities"
    "net/rpc"
    "log"
    "net"
    "net/rpc/jsonrpc"
)

const (
    search  = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
    geocode = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
)

type DadataJsonRPC struct{}

func NewDadataJsonRPC() {
    inbound, err := net.Listen("tcp", "localhost:1234")
    if err != nil {
        log.Fatal(err)
    }
    listener := new(DadataJsonRPC)
    rpc.Register(listener)

    log.Println("Json RPC сервер запущен на порту 1234")
    for {
        conn, err := inbound.Accept()
        if err != nil {
            continue
        }
        jsonrpc.ServeConn(conn)
    }
}

func (d *DadataJsonRPC) Search(query entities.SearchRequest, addr *entities.Addresses) error {
    client := &http.Client{}
    jsonData, err := json.Marshal(query)
    if err != nil {
        return err
    }
    var data = strings.NewReader(string(jsonData))

    req, err := http.NewRequest("POST", search, data)
    if err != nil {
        return err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")

    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return err
    }

    return nil
}

func (d *DadataJsonRPC) Geocode(query entities.GeocodeRequest, addr *entities.Addresses) error {
    client := &http.Client{}
    jsonData, err := json.Marshal(query)
    if err != nil {
        return err
    }

    var data = strings.NewReader(string(jsonData))
    req, err := http.NewRequest("POST", geocode, data)
    if err != nil {
        return err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return err
    }

    return nil
}
