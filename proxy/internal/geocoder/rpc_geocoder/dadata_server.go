package rpc_geocoder

import (
    "net/http"
    "strings"
    "encoding/json"
    "gitlab.com/Icon_ka/geo/internal/entities"
    "net/rpc"
    "log"
    "net"
)

const (
    search  = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
    geocode = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
)

type DadataRPC struct{}

func NewDadataRPC() {
    dadata := new(DadataRPC)
    rpc.Register(dadata)

    l, err := net.Listen("tcp", ":1234")
    if err != nil {
        log.Fatal("Ошибка при запуске сервера:", err)
    }

    log.Println("Rpc сервер запущен на порту 1234")
    rpc.Accept(l)
}

func (d *DadataRPC) Search(query entities.SearchRequest, addr *entities.Addresses) error {
    client := &http.Client{}
    jsonData, err := json.Marshal(query)
    if err != nil {
        log.Print("jsonData, err := json.Marshal(request)", err)
        return err
    }
    var data = strings.NewReader(string(jsonData))

    req, err := http.NewRequest("POST", search, data)
    if err != nil {
        log.Print("req, err := http.NewRequest(\"POST\", search, data)", err)
        return err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")

    resp, err := client.Do(req)
    if err != nil {
        log.Print("resp, err := client.Do(req)", err)
        return err
    }
    defer resp.Body.Close()

    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        log.Print("err = json.NewDecoder(resp.Body).Decode(&addr)", err)
        return err
    }

    return nil
}

func (d *DadataRPC) Geocode(query entities.GeocodeRequest, addr *entities.Addresses) error {
    client := &http.Client{}
    jsonData, err := json.Marshal(query)
    if err != nil {
        return err
    }

    var data = strings.NewReader(string(jsonData))
    req, err := http.NewRequest("POST", geocode, data)
    if err != nil {
        return err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return err
    }

    return nil
}
