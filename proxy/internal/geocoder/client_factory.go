package geocoder

import (
    "gitlab.com/Icon_ka/geo.git/protos/gen/go/dadata"
    "gitlab.com/Icon_ka/geo/internal/entities"
    "context"
    "bytes"
    "encoding/json"
    "log"
    "google.golang.org/grpc"
    "net/rpc"
    "net/rpc/jsonrpc"
)

type GeoProvider interface {
    AddressSearch(input string) (*entities.Addresses, error)
    GeoCode(lat, lng string) (*entities.Addresses, error)
}

type ClientFactory interface {
    CreateClient() GeoProvider
}

type GRPCClientFactory struct{}

type GRPCClient struct {
    dadata.DadataGRPCClient
}

func (c *GRPCClient) AddressSearch(input string) (*entities.Addresses, error) {
    addr, err := c.DadataGRPCClient.Search(context.Background(), &dadata.SearchRequest{Query: input})
    if err != nil {
        return nil, err
    }

    var buf bytes.Buffer

    var res entities.Addresses

    err = json.NewEncoder(&buf).Encode(addr)

    if err != nil {
        return nil, err
    }

    err = json.NewDecoder(&buf).Decode(&res)

    if err != nil {
        return nil, err
    }

    return &res, nil
}

func (c *GRPCClient) GeoCode(lat, lng string) (*entities.Addresses, error) {
    addr, err := c.DadataGRPCClient.Geocode(context.Background(), &dadata.GeocodeRequest{
        Lat: lat,
        Lon: lng,
    })

    var buf bytes.Buffer

    var res entities.Addresses

    err = json.NewEncoder(&buf).Encode(addr)

    if err != nil {
        return nil, err
    }

    err = json.NewDecoder(&buf).Decode(&res)

    if err != nil {
        return nil, err
    }

    return &res, nil
}

func (f *GRPCClientFactory) CreateClient() *GRPCClient {
    conn, err := grpc.Dial("localhost:1234", grpc.WithInsecure())
    if err != nil {
        log.Fatalf("Ошибка при подключении к серверу: %v", err)
    }

    log.Print("gRPC connected")
    client := dadata.NewDadataGRPCClient(conn)

    return &GRPCClient{client}
}

type JsonRPCClientFactory struct{}

type JsonRPCClient struct {
    Client *rpc.Client
}

func (c *JsonRPCClient) AddressSearch(input string) (*entities.Addresses, error) {
    var request entities.SearchRequest
    var addresses entities.Addresses
    request.Query = input

    err := c.Client.Call("DadataJsonRPC.Search", request, &addresses)
    if err != nil {
        return nil, err
    }
    return &addresses, nil
}

func (c *JsonRPCClient) GeoCode(lat, lng string) (*entities.Addresses, error) {
    var request entities.GeocodeRequest
    var addresses entities.Addresses
    request.Lat = lat
    request.Lon = lng

    err := c.Client.Call("DadataJsonRPC.Geocode", request, &addresses)
    if err != nil {
        return nil, err
    }
    return &addresses, nil
}

func (f *JsonRPCClientFactory) CreateClient() *JsonRPCClient {
    client, err := jsonrpc.Dial("tcp", "localhost:1234")
    if err != nil {
        log.Fatal("Ошибка при подключении к серверу:", err)
    }

    log.Print("Json RPC connected")

    return &JsonRPCClient{Client: client}
}

type RPCClientFactory struct{}

type RPCClient struct {
    Client *rpc.Client
}

func (c *RPCClient) AddressSearch(input string) (*entities.Addresses, error) {
    var request entities.SearchRequest
    var addresses entities.Addresses
    request.Query = input

    err := c.Client.Call("DadataRPC.Search", request, &addresses)
    if err != nil {
        log.Print("err := c.Client.Call(\"DadataRPC.Search\", request, addresses)", err)
        return nil, err
    }
    return &addresses, nil
}

func (c *RPCClient) GeoCode(lat, lng string) (*entities.Addresses, error) {
    var request entities.GeocodeRequest
    var addresses entities.Addresses
    request.Lat = lat
    request.Lon = lng

    err := c.Client.Call("DadataRPC.Geocode", request, &addresses)
    if err != nil {
        log.Print("err := c.Client.Call(\"DadataRPC.Geocode\", request, addresses)", err)
        return nil, err
    }
    return &addresses, nil
}

func (f *RPCClientFactory) CreateClient() (*RPCClient, error) {
    client, err := rpc.Dial("tcp", "localhost:1234")
    if err != nil {
        log.Fatal("Ошибка при подключении к серверу:", err)
    }

    return &RPCClient{Client: client}, err
}
