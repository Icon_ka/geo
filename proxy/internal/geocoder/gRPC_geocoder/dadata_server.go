package gRPC_geocoder

import (
    "gitlab.com/Icon_ka/geo.git/protos/gen/go/dadata"
    "google.golang.org/grpc"
    "context"
    "encoding/json"
    "net/http"
    "strings"
    "log"
    "net"
    "fmt"
)

const (
    search  = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
    geocode = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address"
)

type dadataGRPC struct {
    dadata.DadataGRPCServer
}

func NewDadatagRPC() {
    listen, err := net.Listen("tcp", "localhost:1234")
    if err != nil {
        log.Fatalf("Ошибка при прослушивании порта: %v", err)
    }
    server := grpc.NewServer()
    Register(server)

    log.Println("Запуск gRPC сервера...")
    if err := server.Serve(listen); err != nil {
        log.Fatalf("Ошибка при запуске сервера: %v", err)
    }
}

func Register(gRPC *grpc.Server) {
    dadata.RegisterDadataGRPCServer(gRPC, &dadataGRPC{})
}

func (d *dadataGRPC) Search(ctx context.Context, request *dadata.SearchRequest) (*dadata.AddressResponse, error) {
    client := &http.Client{}
    jsonData, err := json.Marshal(request)
    if err != nil {
        return nil, err
    }

    var data = strings.NewReader(string(jsonData))

    req, err := http.NewRequest("POST", search, data)
    if err != nil {
        return nil, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")

    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }

    if resp.StatusCode != http.StatusOK {
        log.Println("Error:", resp.Status)
        return nil, fmt.Errorf("unexpected status: %s", resp.Status)
    }
    defer resp.Body.Close()

    var addr dadata.AddressResponse
    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return nil, err
    }

    return &addr, nil
}

func (d *dadataGRPC) Geocode(ctx context.Context, request *dadata.GeocodeRequest) (*dadata.AddressResponse, error) {
    client := &http.Client{}
    jsonData, err := json.Marshal(request)
    if err != nil {

        return nil, err
    }

    var data = strings.NewReader(string(jsonData))
    req, err := http.NewRequest("POST", geocode, data)
    if err != nil {
        return nil, err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Accept", "application/json")
    req.Header.Set("Authorization", "Token 864ecfb76388cdeb4ee1f7215e1eb8272f5d56b7")
    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    var addr dadata.AddressResponse
    err = json.NewDecoder(resp.Body).Decode(&addr)
    if err != nil {
        return nil, err
    }

    return &addr, nil
}
