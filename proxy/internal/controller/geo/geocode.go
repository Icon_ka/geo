package geo

import (
    "encoding/json"
    "errors"
    "net/http"
    "fmt"
    "gitlab.com/Icon_ka/geo/internal/entities"
    "gitlab.com/Icon_ka/geo/internal/infrastructure/responder"
    "gitlab.com/Icon_ka/geo/internal/service"
    "github.com/prometheus/client_golang/prometheus"
    "gitlab.com/Icon_ka/geo/internal/metrics"
    "time"
)

type GeoCodeController struct {
    Responder responder.Responder
    Service   service.Service
    Metrics   *metrics.Metrics
}

func NewGeoCodeController(responder responder.Responder, serv service.Service, metrics *metrics.Metrics) *GeoCodeController {
    return &GeoCodeController{
        Responder: responder,
        Service:   serv,
        Metrics:   metrics,
    }
}

// Geocode godoc
// @Summary     Геокодирование адреса
// @Description Геокодирование адреса
// @Tags        geocode
// @Accept      json
// @Produce     json
// @Param       body body entities.GeocodeRequest true "Запрос на поиск адресов"
// @Success     200 {object} entities.Addresses "Успешный ответ"
// @Router      /api/address/geocode [post]
func (g *GeoCodeController) Geocode(w http.ResponseWriter, r *http.Request) {
    var re entities.GeocodeRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при сериализации JSON: "+err.Error()))
        return
    }

    resp, err := g.Service.AddressService.GeoCode(re)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при десериализации JSON: "+err.Error()))
        return
    }
}

// Search godoc
// @Summary     Поиск адресов
// @Description Поиск адресов по запросу
// @Tags        geocode
// @Accept      json
// @Produce     json
// @Param       body body entities.SearchRequest true "Запрос на поиск адресов"
// @Success     200 {object} entities.Addresses "Успешный ответ"
// @Router      /api/address/search [post]
func (g *GeoCodeController) Search(w http.ResponseWriter, r *http.Request) {
    startTime := time.Now()
    g.Metrics.HandlerCounter.With(prometheus.Labels{"method": r.Method, "path": r.URL.Path}).Inc()

    var re entities.SearchRequest
    err := json.NewDecoder(r.Body).Decode(&re)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при сериализации JSON: "+err.Error()))
        return
    }

    resp, err := g.Service.AddressService.GetAddresses(re)
    if err != nil {
        fmt.Println("Service: ", err)
        g.Responder.ErrorInternal(w, errors.New("Ошибка при запросе к базе данных: "+err.Error()))
        return
    }

    err = json.NewEncoder(w).Encode(resp)
    if err != nil {
        g.Responder.ErrorInternal(w, errors.New("Ошибка при десериализации JSON:"+err.Error()))
        return
    }

    elapsedTime := time.Since(startTime)
    g.Metrics.HandlerTime.With(prometheus.Labels{"method": r.Method, "path": r.URL.Path}).Observe(elapsedTime.Seconds())
}
