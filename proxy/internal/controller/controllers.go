package controller

import (
    "gitlab.com/Icon_ka/geo/internal/controller/geo"
    "gitlab.com/Icon_ka/geo/internal/infrastructure/responder"
    "gitlab.com/Icon_ka/geo/internal/service"
    "gitlab.com/Icon_ka/geo/internal/metrics"
)

type Controllers struct {
    Geocode geo.GeoCoder
}

func NewControllers(responder responder.Responder, serv service.Service, metrics *metrics.Metrics) *Controllers {
    geocodeController := geo.NewGeoCodeController(responder, serv, metrics)
    return &Controllers{
        Geocode: geocodeController,
    }
}
