package metrics

import "github.com/prometheus/client_golang/prometheus"

type Metrics struct {
    HandlerCounter *prometheus.CounterVec
    HandlerTime    *prometheus.HistogramVec
    DatabaseTime   *prometheus.HistogramVec
    CacheTime      *prometheus.HistogramVec
    DadataTime     *prometheus.HistogramVec
}

func NewMetrics() *Metrics {
    hCounter := NewCounterMetricHandler()
    hTimer := NewTimeMetricHandler()
    dbtimer := NewTimeMetricDatabase()
    cTimer := NewTimeMetricCache()
    dadataTimer := NewTimeMetricDadata()

    prometheus.MustRegister(hCounter, hTimer, dadataTimer, dbtimer, cTimer)
    return &Metrics{
        HandlerCounter: hCounter,
        HandlerTime:    hTimer,
        DatabaseTime:   dbtimer,
        CacheTime:      cTimer,
        DadataTime:     dadataTimer,
    }
}
