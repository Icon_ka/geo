package metrics

import "github.com/prometheus/client_golang/prometheus"

func NewTimeMetricHandler() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name:    "http_request_duration_seconds",
            Help:    "Duration of HTTP requests.",
            Buckets: []float64{.1, .2, .5, 1, 2, 5, 10},
        },
        []string{"method", "path"},
    )

    return requestDuration
}

func NewTimeMetricDatabase() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name:    "database_request_duration_seconds",
            Help:    "Duration of database requests.",
            Buckets: []float64{.1, .2, .5, 1, 2, 5, 10},
        },
        []string{"method", "path"},
    )

    return requestDuration
}

func NewTimeMetricCache() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name:    "cache_request_duration_seconds",
            Help:    "Duration of cache requests.",
            Buckets: []float64{.1, .2, .5, 1, 2, 5, 10},
        },
        []string{"method", "path"},
    )

    return requestDuration
}

func NewTimeMetricDadata() *prometheus.HistogramVec {
    requestDuration := prometheus.NewHistogramVec(
        prometheus.HistogramOpts{
            Name:    "dadata_request_duration_seconds",
            Help:    "Duration of dadata requests.",
            Buckets: []float64{.1, .2, .5, 1, 2, 5, 10},
        },
        []string{"method", "path"},
    )

    return requestDuration
}

func NewCounterMetricHandler() *prometheus.CounterVec {
    requestCounter := prometheus.NewCounterVec(
        prometheus.CounterOpts{
            Name: "http_requests_total",
            Help: "Number of HTTP requests made.",
        },
        []string{"method", "path"},
    )

    return requestCounter
}
