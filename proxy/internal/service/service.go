package service

import (
    "gitlab.com/Icon_ka/geo/internal/service/geo"
    "database/sql"
    "gitlab.com/Icon_ka/geo/internal/metrics"
    "gitlab.com/Icon_ka/geo/internal/geocoder"
)

type Service struct {
    AddressService geo.AddressServicer
}

func NewServices(db *sql.DB, metrics *metrics.Metrics, dadata geocoder.GeoProvider) *Service {
    return &Service{AddressService: geo.NewAddressService(db, metrics, dadata)}
}
