package geo

import (
    "gitlab.com/Icon_ka/geo/internal/entities"
    "database/sql"
    "gitlab.com/Icon_ka/geo/internal/repository/geo"
    "gitlab.com/Icon_ka/geo/internal/metrics"
    "gitlab.com/Icon_ka/geo/internal/repository/cache"
    "sync"
    "gitlab.com/Icon_ka/geo/internal/geocoder"
)

type AddressService struct {
    cache  cache.PostgresAddressRepositoryProxy
    Dadata geocoder.GeoProvider
    mutex  sync.Mutex
}

func NewAddressService(pg *sql.DB, metrics *metrics.Metrics, dadata geocoder.GeoProvider) *AddressService {
    repo := geo.NewPostgresAddressRepository(pg, metrics)
    return &AddressService{
        cache:  *cache.NewPostgresAddressRepositoryProxy(repo, metrics),
        Dadata: dadata,
    }
}

func (a *AddressService) GetAddresses(req entities.SearchRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()

    address, err := a.cache.GetAddresses(req.Query)

    if err != nil {
        if err == sql.ErrNoRows {
            addr, err := a.Dadata.AddressSearch(req.Query)
            if err != nil {
                return nil, err
            }

            var addressDTO entities.AddressDTO
            addressDTO.Data = addr.Suggestions[0].Value
            addressDTO.Lat = addr.Suggestions[0].Data.Lat
            addressDTO.Lon = addr.Suggestions[0].Data.Lon

            err = a.cache.Repository.SaveAddresses(addressDTO, req.Query)
            if err != nil {
                return nil, err
            }

            return addr, nil
        }
        return nil, err
    }
    var addressResponse entities.Addresses
    s := make([]entities.Suggestion, 1)
    s[0].Value = address.Data
    s[0].Data.Lat = address.Lat
    s[0].Data.Lon = address.Lon
    addressResponse.Suggestions = s

    return &addressResponse, nil
}

func (a *AddressService) GeoCode(req entities.GeocodeRequest) (*entities.Addresses, error) {
    a.mutex.Lock()
    defer a.mutex.Unlock()

    addr, err := a.Dadata.GeoCode(req.Lat, req.Lon)
    if err != nil {
        return nil, err
    }

    return addr, nil
}
