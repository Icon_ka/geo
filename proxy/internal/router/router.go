package router

import (
    "github.com/go-chi/chi/v5"
    "github.com/go-chi/chi/v5/middleware"

    "net/http"

    modules "gitlab.com/Icon_ka/geo/internal/controller"

    "net/http/httputil"

    "fmt"

    "net/url"

    _ "net/http/pprof"
    "github.com/prometheus/client_golang/prometheus/promhttp"
    _ "gitlab.com/Icon_ka/geo/docs"
)

func NewApiRouter(controllers *modules.Controllers) http.Handler {
    r := chi.NewRouter()

    proxy := NewReverseProxy("hugo", "1313")

    r.Use(middleware.Logger, proxy.ReverseProxy)

    r.Mount("/debug", middleware.Profiler())

    r.Handle("/metrics", promhttp.Handler())

    r.Get("/swagger", swaggerUI)

    r.Post("/api/address/search", controllers.Geocode.Search)
    r.Post("/api/address/geocode", controllers.Geocode.Geocode)

    return r
}

type ReverseProxy struct {
    host string
    port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
    return &ReverseProxy{
        host: host,
        port: port,
    }
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
    target, _ := url.Parse(fmt.Sprintf("http://%s:%s", rp.host, rp.port))
    proxy := httputil.NewSingleHostReverseProxy(target)
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if r.URL.Path == "/api/address/search" ||
            r.URL.Path == "/api/address/geocode" ||
            r.URL.Path == "/debug" ||
            r.URL.Path == "/debug/pprof/" ||
            r.URL.Path == "/debug/pprof/allocs" ||
            r.URL.Path == "/debug/pprof/block" ||
            r.URL.Path == "/debug/pprof/cmdline" ||
            r.URL.Path == "/debug/pprof/goroutine" ||
            r.URL.Path == "/debug/pprof/heap" ||
            r.URL.Path == "/debug/pprof/mutex" ||
            r.URL.Path == "/debug/pprof/profile" ||
            r.URL.Path == "/debug/pprof/threadcreate" ||
            r.URL.Path == "/debug/pprof/trace" ||
            r.URL.Path == "/metrics" ||
            r.URL.Path == "/swagger" {
            next.ServeHTTP(w, r)
        } else {
            proxy.ServeHTTP(w, r)
        }
    })
}
