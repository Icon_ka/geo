CREATE TABLE search_history (
                       id SERIAL PRIMARY KEY,
                       query VARCHAR(255) NOT NULL
);

CREATE TABLE address (
                         id SERIAL PRIMARY KEY,
                         data VARCHAR(255) NOT NULL,
                         lat VARCHAR(255) NOT NULL,
                         lon VARCHAR(255) NOT NULL
);


CREATE TABLE history_search_address (
                                        search_history_id INTEGER NOT NULL,
                                        address_id INTEGER NOT NULL,
                                        PRIMARY KEY (search_history_id, address_id)
);

CREATE TABLE users (
                         id SERIAL PRIMARY KEY,
                         username VARCHAR(255) NOT NULL,
                         password_hash BYTEA NOT NULL,
                         token VARCHAR(255)
);

ALTER TABLE history_search_address
    ADD CONSTRAINT fk_search_history
    FOREIGN KEY (search_history_id)
    REFERENCES search_history (id);

ALTER TABLE history_search_address
    ADD CONSTRAINT fk_address
    FOREIGN KEY (address_id)
    REFERENCES address (id);


CREATE EXTENSION pg_trgm;